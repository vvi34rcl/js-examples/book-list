function Book(title, author, isbn) {
    this.title = title;
    this.author = author;
    this.isbn = isbn;
}


function BookList(element) {
    this.element = document.querySelector(element);
}

BookList.prototype.onRemoveButtonClick = function(event) {
    this.element.removeChild(event.target.parentNode.parentNode);
    this.onBookRemove();
};

BookList.prototype.add = function(book) {
    const row = document.createElement('tr');

    const titleCell = document.createElement('td');
    titleCell.appendChild(document.createTextNode(book.title));
    row.appendChild(titleCell);

    const authorCell = document.createElement('td');
    authorCell.appendChild(document.createTextNode(book.author));
    row.appendChild(authorCell);

    const isbnCell = document.createElement('td');
    isbnCell.appendChild(document.createTextNode(book.isbn));
    row.appendChild(isbnCell);

    const removeCell = document.createElement('td');
    const removeLink = document.createElement('a'); // Должна быть кнопка
    removeLink.href = 'javascript: void 0';
    removeLink.appendChild(document.createTextNode('×'));
    removeLink.addEventListener('click', (event) => this.onRemoveButtonClick(event));
    removeCell.appendChild(removeLink);
    row.appendChild(removeCell);

    this.element.appendChild(row);
}


function AddBookForm(element) {
    this.element = document.querySelector(element);

    this.titleInput = document.getElementById('title');
    this.authorInput = document.getElementById('author');
    this.isbnInput = document.getElementById('isbn');

    this.element.addEventListener('submit', (event) => this.onSubmit(event));
}

AddBookForm.prototype.clearFields = function() {
    this.titleInput.value = '';
    this.authorInput.value = '';
    this.isbnInput.value = '';
}


function Application() {
    this.addBookForm = new AddBookForm('#add-book-form');
    this.bookList = new BookList('#book-list');

    this.addBookForm.onSubmit = (event) => {
        event.preventDefault();

        const title = this.addBookForm.titleInput.value;
        const author = this.addBookForm.authorInput.value;
        const isbn = this.addBookForm.isbnInput.value;

        if (title === '' || author === '' || isbn === '') {
            this.showAlert('error', 'Please fill in all fields');

            return;
        }

        const newBook = new Book(title, author, isbn);
        this.bookList.add(newBook);
        this.addBookForm.clearFields();
        this.showAlert('success', 'Book added!');
    }

    this.bookList.onBookRemove = () => {
        this.showAlert('success', 'Book removed!');
    }
}

Application.prototype.showAlert = function(type, messageText) {
    const alertBlock = document.createElement('div');
    alertBlock.className = `alert ${type}`;
    alertBlock.appendChild(document.createTextNode(messageText));
    this.addBookForm.element.parentNode.insertBefore(alertBlock, this.addBookForm.element);

    setTimeout(function() {
        alertBlock.remove();
    }, 3000);
}


new Application();
