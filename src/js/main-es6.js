class Book {
    constructor(title, author, isbn) {
        this.title = title;
        this.author = author;
        this.isbn = isbn;
    }
}


class AddBookForm {
    constructor(elementSelector) {
        this.element = document.querySelector(elementSelector);

        this.titleInput = document.getElementById('title');
        this.authorInput = document.getElementById('author');
        this.isbnInput = document.getElementById('isbn');

        this.element.addEventListener('submit', (event) => this.onSubmit(event));
    }

    clearFields() {
        this.titleInput.value = '';
        this.authorInput.value = '';
        this.isbnInput.value = '';
    }
}


class BookList {
    constructor(elementSelector) {
        this.element = document.querySelector(elementSelector);
    }

    onRemoveButtonClick(event) {
        const row = event.target.parentNode.parentNode;

        this.element.removeChild(row);

        this.onBookRemove(row.book);  // Должно быть изменено на fireEvent('bookremove')
    }

    add(book) {
        const row = document.createElement('tr');

        const titleCell = document.createElement('td');
        titleCell.appendChild(document.createTextNode(book.title));
        row.appendChild(titleCell);

        const authorCell = document.createElement('td');
        authorCell.appendChild(document.createTextNode(book.author));
        row.appendChild(authorCell);

        const isbnCell = document.createElement('td');
        isbnCell.appendChild(document.createTextNode(book.isbn));
        row.appendChild(isbnCell);

        const removeCell = document.createElement('td');
        const removeLink = document.createElement('a'); // Должна быть кнопка
        removeLink.href = 'javascript: void 0';
        removeLink.appendChild(document.createTextNode('×'));
        removeLink.addEventListener('click', (event) => this.onRemoveButtonClick(event));
        removeCell.appendChild(removeLink);
        row.appendChild(removeCell);

        row.book = book;

        this.element.appendChild(row);
    }
}


class BookStorage {
    constructor() {
        this._books = localStorage.getItem('bookList');
        this._books = this._books === null ? [] : this._books = JSON.parse(this._books);
    }

    addBook(book) {
        this._books.push(book);
        localStorage.setItem('bookList', JSON.stringify(this._books));
    }

    getBooks() {
        return this._books;
    }

    removeBook(book) {
        this._books.splice(this._books.indexOf(book), 1);
        localStorage.setItem('bookList', JSON.stringify(this._books));
    }
}


class Application {
    constructor() {
        this.bookStorage = new BookStorage();
        this.addBookForm = new AddBookForm('#add-book-form');
        this.bookList = new BookList('#book-list');

        this.bookStorage.getBooks().forEach((book) => {
            this.bookList.add(book);
        });

        this.addBookForm.onSubmit = (event) => {
            event.preventDefault();

            const title = this.addBookForm.titleInput.value;
            const author = this.addBookForm.authorInput.value;
            const isbn = this.addBookForm.isbnInput.value;

            if (title === '' || author === '' || isbn === '') {
                this.showAlert('error', 'Please fill in all fields');

                return;
            }

            const newBook = new Book(title, author, isbn);
            this.bookList.add(newBook);
            this.addBookForm.clearFields();
            this.bookStorage.addBook(newBook);
            this.showAlert('success', 'Book added!');
        }

        this.bookList.onBookRemove = (book) => {
            this.bookStorage.removeBook(book);
            this.showAlert('success', 'Book removed!');
        }
    }

    showAlert(type, messageText) {
        const alertBlock = document.createElement('div');
        alertBlock.className = `alert ${type}`;
        alertBlock.appendChild(document.createTextNode(messageText));
        this.addBookForm.element.parentNode.insertBefore(alertBlock, this.addBookForm.element);

        setTimeout(function() {
            alertBlock.remove();
        }, 3000);
    }
}


new Application();
